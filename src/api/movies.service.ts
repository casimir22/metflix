import { ObjectId } from "bson"
import { Collection, Db, MongoClient } from "mongodb"
import { Movie } from "../models/movie";

var db: Db;
var collections: { movies?: Collection<Movie> } = {};

export default class MoviesService {

    static async injectDB(client: MongoClient) {
        if (db) {
            return;
        }
        try {
            await client.connect();

            db = client.db(process.env.MFLIX_DB_NAME);

            collections.movies = db.collection(process.env.MOVIES_COLLECTION_NAME || "");

        } catch (e) {
            console.error(e);
            throw `No se puedo establecer una conexión con la bd en MoviesService: ${e}`;
        }
    }

    static async getById(id: string) {
        if (!collections.movies)
            return null;

        try {
            let movie = collections.movies?.findOne({ _id: new ObjectId(id) });
            return movie;
        } catch (e) {
            throw "Algo va mal";
        }
    }

    static async filterByGenres(countries: Array<string>, genres: Array<string>) {

        let cursor = await collections.movies?.find({ genres: { $in: genres } }).limit(10);
        // let cursor1 = await collections.movies?.find({awards: { $in: 2000 } }).limit(10);

        if ((await cursor?.count()) === 0)
            return [];

        let movies: Array<Movie> = [];

        await cursor?.forEach(c => {
            movies.push(c as Movie);
        });

        return movies;
    }
    static async filterByYear(year: number) {

        let cursor = await collections.movies?.find({ year: year }).limit(10);

        // if ((await cursor?.count()) === 0)
        //     return [];

        let movies: Array<Movie> = [];

        await cursor?.forEach(c => {
            movies.push(c as Movie);
        });

        return movies;
    }

}
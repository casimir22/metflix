import express, { Request, Response } from "express";
import moviesService from "./movies.service";

const router = express.Router();

router.use(express.json());

router.get("/id/:id", async (req: Request, res: Response) => {

    const id = req?.params?.id;
    try {
        let movie = await moviesService.getById(id);
        if (!movie)
            res.status(404).send(`No se encontró el película con id: ${id}`);
        else
            res.json(movie);
    } catch (error) {
        res.status(404).send(`No se encontró el película con id: ${id}`);
    }
});

router.get("/findByGenres", async (req: Request, res: Response) => {

    let countries = typeof req?.query?.countries === "string" ? req.query.countries.split(",") : [];
    let genres = typeof req?.query?.genres === "string" ? req.query.genres.split(",") : [];

    // consulta si countries es un array
    if (!(countries instanceof Array) || !(genres instanceof Array)) 
        res.status(400).send();

    try {
            let movies = await moviesService.filterByGenres([], genres)
            res.json(movies);
            
    }
    catch (error) {
        res.status(404).send(`No se encontrarn pelíulas`);
    }
});

router.get("/findByCountries", async (req: Request, res: Response) => {

    let countries = typeof req?.query?.countries === "string" ? req.query.countries.split(",") : [];
    let genres = typeof req?.query?.genres === "string" ? req.query.genres.split(",") : [];

    // consulta si countries es un array
    if (!(countries instanceof Array) || !(genres instanceof Array)) 
        res.status(400).send();

    try {
            let movies = await moviesService.filterByGenres(countries, genres)
            res.json(movies);
    }
    catch (error) {
        res.status(404).send(`No se encontrarn pelíulas`);
    }
});

router.get("/findByYear", async (req: Request, res: Response) => {

    let year = typeof req?.query?.year === "string" ? parseInt(req.query.year) : null;
    
    if(!year){
        res.status(400).send("Ingrese un año válido");
        return 
    }

    try {
        let movies = await moviesService.filterByYear(year)
        res.json(movies);
    }
    catch (error) {
        res.status(404).send(`No se encontrarn pelíulas`);
    }
});
export default router;

import express from "express";
import { MongoClient } from "mongodb"
import productosService from "./api/productos.service"
import productosRouter from "./api/productos.controller";
import moviesService from "./api/movies.service";
import moviesRouter from "./api/movies.controller";
import fs from "fs";
import * as dotenv from "dotenv";

dotenv.config();

const port = process.env.PORT;

const app = express();
app.use(express.json());

app.use("/productos", productosRouter);
app.use("/movies", moviesRouter);

app.get("/", async (req, res) => {
    fs.readFile("./archivos/banner.txt", "ascii", (err, data) => {
        if (err)
            res.send("NodeJS + Express + MongoDB");
        else
            res.send(data.toString());
    });
});

MongoClient.connect(
    process.env.DB_URI || "mongodb+srv://Daniela:zeLkPLyaIcE4fZJ6@cluster0.rgrht.mongodb.net",
    // TODO: Connection Pooling
    // Set the poolSize to 50 connections.
    // TODO: Timeouts
    // Set the write timeout limit to 2500 milliseconds.
).catch(err => {
    console.error(err.stack)
    process.exit(1)
}).then(async client => {
    await productosService.injectDB(client);
    await moviesService.injectDB(client);
    app.listen(port, () => {
        console.log(`Server corriendo en http://localhost:${port}`)
    })
});